<?php

return [
  'database' => [
    'name' => 'mytodo',
    'username' => 'admin',
    'password' => 'admin',
    'connection' => 'mysql:host=db:3306',
    'options' => [
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]
  ]
];
